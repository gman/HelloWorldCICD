Master: 
[![pipeline status](https://framagit.org/gman/HelloWorldCICD/badges/master/pipeline.svg)](https://framagit.org/gman/HelloWorldCICD/commits/master) [![coverage report](https://framagit.org/gman/HelloWorldCICD/badges/master/coverage.svg)](https://framagit.org/gman/HelloWorldCICD/commits/master)

Test:
[![pipeline status](https://framagit.org/gman/HelloWorldCICD/badges/Test/pipeline.svg)](https://framagit.org/gman/HelloWorldCICD/commits/Test)    [![coverage report](https://framagit.org/gman/HelloWorldCICD/badges/Test/coverage.svg)](https://framagit.org/gman/HelloWorldCICD/commits/Test)

Development:
[![pipeline status](https://framagit.org/gman/HelloWorldCICD/badges/Dev/pipeline.svg)](https://framagit.org/gman/HelloWorldCICD/commits/Dev)    [![coverage report](https://framagit.org/gman/HelloWorldCICD/badges/Dev/coverage.svg)](https://framagit.org/gman/HelloWorldCICD/commits/Dev)

This is a simple project to test the ci/cd function
See the wiki for some useful notes
