'''
Created on 1 Jun 2018

@author: gaetanmatthys
'''


def say_hello():
    return('Hello World')


if __name__ == '__main__':
    print(say_hello())
