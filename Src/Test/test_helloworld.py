from HelloWorld import helloworld


def test_say_hello():
    assert helloworld.say_hello() == 'Hello World'
